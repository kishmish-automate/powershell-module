# Powershell-Module

This is a powershell 4+ module to manipulate the Automate service. Includes the following commands

**Get-LTServiceInfo**
    <br>This function will pull all of the registry data into an object. Also displays current registry data

**Get-LTServiceInfoBackup**
    <br>This function will pull all of the backed up registry data into an object.
    
**Get-LTServiceSettings**
    <br>This function will pull the registry data from HKLM:\SOFTWARE\LabTech\Service\Settings into an object.

**Install-LTService**
    <br>This function will install the Automate agent on the machine.

Arguments

    >  -Server "https://lt.kishmish.com"
    >  -LocationID 44
    >  -ServerPassword "passwordinplaintext"

**Invoke-LTServiceCommand**
    This function will call the specified agent command
    
**New-LTServiceBackup**
    This function will backup all the reg keys to 'HKLM\SOFTWARE\LabTechBackup'
    This will also backup those files to "$((Get-LTServiceInfo).BasePath)Backup"
    
**Reinstall-LTService**
    This will ReInstall the Automate agent using the server address in the registry.

**Reset-LTService**
    This function will remove local settings on the agent.

**Restart-LTService**
    This function will restart the Automate Services.
    
**Start-LTService**
    This function will start the Automate Services

**Stop-LTService**
    This Function will stop the Automate Services
    
**Uninstall-LTService**
    This function will remove the Automate services
    
**Update-LTService**
    This function will manually update the Automate agent to the requested version.

